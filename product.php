<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello technodom</title>
    <link rel="stylesheet" href="./asset/css/styles.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
</head>
<body>
<section class="section">
    <div class="container">

        <?php
        include "api/product.php";
        $d = DvdDisk::class;
        $b = Book::class;
        $f = Furniture::class;

        $products = [];
        array_push($products, $d);
        array_push($products, $b);
        array_push($products, $f);
        ?>
        <!-- Main container -->
        <form action="#" id="myForm" method="post">
        <nav class="level">
            <!-- Left side -->
            <div class="level-left">
                <div><h1>Product Add</h1></div>
            </div>

            <!-- Right side -->
            <div class="level-right">
                <p class="level-item"><a href="index.php" class="button is-dark">Back</a></p>
                <p class="level-item"><button type="submit" class="button is-success">Save</button></p>
            </div>
        </nav>
            <div class="columns ">

                <div class="column">

                    <div class="field  is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">SKU</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input class="input" name="sku" type="text" placeholder="SKU">
                                </div>
                                <p class="help is-danger"></p>
                            </div>
                        </div>
                    </div>

                    <div class="field  is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Name</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input class="input" name="name" type="text" placeholder="Name">
                                </div>
                                <p class="help is-danger"></p>
                            </div>
                        </div>
                    </div>

                    <div class="field  is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Price</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <input class="input" name="price" type="number" placeholder="Price">
                                </div>
                                <p class="help is-danger"></p>
                            </div>
                        </div>
                    </div>

                    <div class="field  is-horizontal">
                        <div class="field-label is-normal">
                            <label class="label">Type</label>
                        </div>
                        <div class="field-body">
                            <div class="field">
                                <div class="control">
                                    <div class="select">
                                        <select name="type" onchange="if (this.selectedIndex) selectProductType(this);">
                                            <option value="-1">...</option>
                                            <?php
                                            foreach ($products as $v)
                                            {
                                                print "<option value='{$v}'>". $v ."</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <p class="help is-danger"></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column" id="dynamic"></div>

            </div>
            <div class="notification is-danger" id="output" style="display: none;">

            </div>
        </form>

    </div>
</section>
<script type="text/javascript">
    let myArray = {
        <?php
        print DvdDisk::class . ":'" . str_replace(array("\n", "\r"), '',DvdDisk::getAttributeInput()) . "',";
        print Book::class . ":'" . str_replace(array("\n", "\r"), '',Book::getAttributeInput()) . "',";
        print Furniture::class . ":'" . str_replace(array("\n", "\r"), '',Furniture::getAttributeInput()) . "',";
        ?>
    };
    function selectProductType(element) {
        document.getElementById('dynamic').innerHTML = myArray[element.value];
    }

    function toJSONString( form ) {
        let obj = {};
        let elements = form.querySelectorAll( "input, select, textarea" );
        for( let i = 0; i < elements.length; ++i ) {
            let element = elements[i];
            let name = element.name;
            let value = element.value;

            if( name ) {
                obj[ name ] = value;
            }
        }
        return JSON.stringify( obj );
    }

    function sendData(data){
        let output = document.getElementById("output");
        let xhr = new XMLHttpRequest();
        xhr.open('POST', './api/index.php', true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onload = function () {
            // do something to response
            if (this.responseText !== ""){
                output.innerHTML = this.responseText;
                output.style.display = 'block';
            } else {
                output.style.display = 'none';
                window.location.pathname = 'index.php';
            }

        };
        xhr.send(data);

    }

    document.addEventListener( "DOMContentLoaded", function() {
        let form = document.getElementById("myForm");
        let output = document.getElementById("output");
        form.addEventListener("submit", function( e ) {
            e.preventDefault();
            let json = toJSONString(form);
            sendData(json);
        }, false);

    });
</script>
</body>
</html>