<?php

class DataBase
{

    private static $dir = "/entity/";

    public static function creatEntity($data, $class)
    {
        file_put_contents(self::getEntityName($data['sku'], $class), json_encode($data));
    }

    public static function findEntity($sku)
    {
        $json = file_get_contents($sku);
        return json_decode($json, true);
    }

    public static function findAllEntity(){
        $files = glob($_SERVER['DOCUMENT_ROOT'] . "/entity/*");
        $ar = [];
        foreach ($files as $f)
        {

            $string  = file_get_contents($f);
            $data = json_decode($string, true);
            $className = $data["type"];

            $object = new $className($data);
            array_push($ar, $object);
        }
        return $ar;
    }

    public static function deleteEntityAll($array){
        foreach ($array  as $value){
            self::deleteEntity($value);
        }
    }

    private static function deleteEntity($sku){
        if(self::entityExists($sku))
        {
            unlink(self::getEntityName($sku));
        }
    }

    private static function getEntityName($sku)
    {
        return $_SERVER['DOCUMENT_ROOT'] . self::$dir . strtolower($sku);
    }

    public static function entityExists($sku)
    {
        $file = self::getEntityName($sku);
        if (file_exists($file)) {
            return true;
        } else {
            return false;
        }
    }
}
