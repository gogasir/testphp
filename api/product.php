<?php

interface PersonInterface
{
    public function printHtml();

    public static function getAttributeInput();

    public function set($data);
}

abstract class Product implements PersonInterface
{
    public $sku;
    public $name;
    public $price;

    public function set($data)
    {
        foreach ($data as $key => $value) $this->{$key} = $value;
    }

    public function __construct($data)
    {
        foreach ($data as $key => $value) $this->{$key} = $value;
    }

    public function validate()
    {
        if (!isset($this->sku) || !isset($this->name) || !isset($this->price)) {
            return false;
        }
        if (empty($this->sku) || empty($this->name) || empty($this->price)) {
            return false;
        }
        return true;
    }
}

class DvdDisk extends Product
{
    public $size;

    public function printHtml()
    {
        print "<p class='bd-notification'>$this->sku</p>";
        print "<p>$this->name</p>";
        print "<p>{$this->price} тенге</p>";
        print "<p>Size: {$this->size}</p>";
    }

    public function validate()
    {
        $result = parent::validate();
        if (!$result) return false;
        if (!isset($this->size) || empty($this->size)) {
            return false;
        }
        return true;
    }

    public static function getAttributeInput()
    {
        return <<<TAG
<div class="field  is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Size</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <input class="input" name="size" id="id-size" type="number" placeholder="Size">
                            </div>
                            <p class="help is-danger"></p>
                        </div>
                    </div>
                </div>
<p><b>DVDDisk</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec justo quam, id tincidunt dolor. Integer a tellus ut nunc elementum volutpat. Vivamus eu arcu lacus. Phasellus eu ipsum a metus ullamcorper mollis. Sed eget ligula enim. </p>
TAG;
    }


}

class Book extends Product
{
    public $weight;

    public function printHtml()
    {
        print "<p>$this->sku</p>";
        print "<p>$this->name</p>";
        print "<p>{$this->price} тенге</p>";
        print "<p>Weight: {$this->weight}</p>";
    }

    public function validate()
    {
        $result = parent::validate();
        if (!$result) return false;
        if (!isset($this->weight) || empty($this->weight)) {
            return false;
        }
        return true;
    }

    public static function getAttributeInput()
    {
        return <<<TAG
<div class="field  is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Size</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <input class="input" name="weight" id="id-weight" type="number" placeholder="Weight">
                            </div>
                            <p class="help is-danger"></p>
                        </div>
                    </div>
                </div>
<p><b>Book</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec justo quam, id tincidunt dolor. Integer a tellus ut nunc elementum volutpat. Vivamus eu arcu lacus. Phasellus eu ipsum a metus ullamcorper mollis. Sed eget ligula enim.</p> 
TAG;
    }


}

class Furniture extends Product
{
    public $height;
    public $wight;
    public $length;

    function printHtml()
    {
        print "<p>$this->sku</p>";
        print "<p>$this->name</p>";
        print "<p>{$this->price} тенге</p>";
        print "<p>Dimensions: {$this->height}X{$this->wight}X{$this->length}</p>";
    }

    public function validate()
    {
        $result = parent::validate();
        if (!$result) return false;
        if (!isset($this->height) ||
            empty($this->height) ||
            !isset($this->wight) ||
            empty($this->wight) ||
            !isset($this->length) ||
            empty($this->length)) {
            return false;
        }
        return true;
    }

    public static function getAttributeInput()
    {
        return <<<TAG
<div class="field  is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Dimensions:</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <div class="columns">
                                <div class="column">
                                    <label>Height</label>
                                    <input class="input" name="height" id="id-height" type="number" placeholder="Height">
                                </div>
                                <div class="column">
                                    <label>Wight</label>
                                    <input class="input" name="wight" id="id-wight" type="number" placeholder="Wight">
                                </div>
                                <div class="column">
                                <label>Length</label>
                                    <input class="input" name="length" id="id-length" type="number" placeholder="Length">
                                </div>
</div>
                                
                            </div>
                            <p class="help is-danger"></p>
                        </div>
                    </div>
                </div>
<p><b>Furniture</b>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas nec justo quam, id tincidunt dolor. Integer a tellus ut nunc elementum volutpat. Vivamus eu arcu lacus. Phasellus eu ipsum a metus ullamcorper mollis. Sed eget ligula enim.</p> 
TAG;

    }


}