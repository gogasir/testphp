<?php

include "product.php";
include "db.php";

class ApiController
{
    private $requestMethod;
    private $url;

    public function __construct($requestMethod, $url)
    {
        $this->requestMethod = $requestMethod;
        $this->url = $url;
    }

    public function processRequest()
    {
        switch ($this->url) {
            case 'index.php':
                if ($this->requestMethod ==  'POST'){
                        $response = $this->createProductFromRequest();
                }
                break;
            case 'delete.php':
                if ($this->requestMethod ==  'POST'){
                    $response = $this->deleteProductFromRequest();
                }
                break;
        }

        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    private function deleteProductFromRequest(){
        $input = (array) explode(",",file_get_contents('php://input'));
        $a = [];
        foreach ($input as $value){
            array_push($a, $value);
        }

        DataBase::deleteEntityAll($a);

        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = "success";
        return $response;
    }

    private function createProductFromRequest()
    {
        $input = (array)json_decode(file_get_contents('php://input'), TRUE);

        if ($input["type"] == -1){
            return $this->unprocessableEntityResponse('Fill in all fields');
        }

        if (!$this->validateProduct($input)) {
            return $this->unprocessableEntityResponse('Fill in all fields');
        }
        if (DataBase::entityExists($input['sku'])) {
            return $this->unprocessableEntityResponse('The product exists');
        }

        DataBase::creatEntity($input, $input['type']);
        $response['status_code_header'] = 'HTTP/1.1 201 OK';
        $response['body'] = "";
        return $response;
    }

    private function validateProduct(array $input)
    {
        $className = $input["type"];
        $object = new $className($input);
        return $object->validate();
    }

    private function unprocessableEntityResponse($text)
    {
        $response['status_code_header'] = 'HTTP/1.1 422 Unprocessable Entity';
        $response['body'] = $text;
        return $response;
    }
}