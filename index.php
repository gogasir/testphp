<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hello technodom</title>
    <link rel="stylesheet" href="./asset/css/styles.css">
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
    <style type="text/css">
        .box p{
            text-align: center;
        }
    </style>
</head>
<body>
<section class="section">
    <div class="container">
        <?php
        include "api/product.php";
        include "api/db.php";
        $products = DataBase::findAllEntity();
        ?>
        <!-- Main container -->
        <nav class="level">
            <!-- Left side -->
            <div class="level-left">
                <div><h1>Product List</h1></div>
            </div>

            <!-- Right side -->
            <div class="level-right">
                <div class="level-item">
                <div class="field  is-horizontal">
                    <div class="field-label is-normal">
                        <label class="label">Action</label>
                    </div>
                    <div class="field-body">
                        <div class="field">
                            <div class="control">
                                <div class="select">
                                    <select id="actionCode">
                                        <option value="0">Creat product</option>
                                        <option value="1">Delete select</option>
                                    </select>
                                </div>
                            </div>
                            <p class="help is-danger"></p>
                        </div>
                    </div>
                </div>
                </div>
                <p class="level-item"><button type="button" onclick="actionButtom()" class="button is-success">Apply</button></p>
            </div>
        </nav>


            <?php
            foreach ($products as $key=>$value) {
                if($key%4 === 0) echo '<div class="columns is-centered">';
                print "<div class=\"column is-one-quarter\"><div class=\"box\"><label class=\"checkbox\"><input type=\"checkbox\" value=\"{$value->sku}\"></label>";
                print  $value->printHtml();
                print "</div></div>";
                if($key%4 === 3) echo '</div>';
            }
            ?>

    </div>
</section>
<script type="text/javascript">

    function actionButtom(){
        let e = document.querySelector('#actionCode');
        let value = e.options[e.selectedIndex].value;
        if (value == 0) {
            window.location.pathname = 'product.php';
        } else {
            deleteSelected();
        }
    }

    function deleteSelected() {
        let array = [];
        let chekboxes = document.querySelectorAll('input[type=checkbox]:checked');
        for (var i = 0; i < chekboxes.length; i++) {
           array.push(chekboxes[i].value);
        }
        if (array.length > 0) {
            sendData(array);
        }

    }

    function sendData(data){
        let output = document.getElementById("output");
        let xhr = new XMLHttpRequest();
        xhr.open('POST', './api/delete.php', true);
        xhr.setRequestHeader('Content-type', 'application/json');
        xhr.onload = function () {
           window.location.pathname = 'index.php';
        };
        xhr.send(data);
    }
</script>
</body>
</html>